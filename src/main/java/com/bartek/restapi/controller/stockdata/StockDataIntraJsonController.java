package com.bartek.restapi.controller.stockdata;

import com.bartek.model.morphia.StockDataIntraJson;
import com.bartek.restapi.repository.stockdata.StockDataIntraJsonJsonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/stock-data")
public class StockDataIntraJsonController
{

    @Autowired StockDataIntraJsonJsonRepository stockDataIntraJsonRepository;

    @GetMapping("/getall")
    public List<StockDataIntraJson> getAll()
    {
        return stockDataIntraJsonRepository.findAll();
    }

}
