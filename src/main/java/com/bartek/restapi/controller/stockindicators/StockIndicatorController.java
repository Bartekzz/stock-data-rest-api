package com.bartek.restapi.controller.stockindicators;

import com.bartek.model.morphia.indicators.StockIndicator;
import com.bartek.restapi.repository.stockindicators.StockIndicatorRepository;
import com.bartek.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;

@RestController
@RequestMapping("/api/stock-indicators")
public class StockIndicatorController
{

    @Autowired StockIndicatorRepository stockIndicatorRepository;

    @GetMapping("/getall")
    public List<StockIndicator> getAll()
    {
        return stockIndicatorRepository.findAll();
    }

    @GetMapping("/getDates/")
    public List<LocalDateTime> findDistinctDateTimes(
            @RequestParam Optional<String> period
    )
    {
        return stockIndicatorRepository.findDistinctDateTimes(period);
    }

    @GetMapping("/getTimes/{date}/{period}")
    public Set<LocalTime>findDistinctTimesForDate(
            @PathVariable String date,
            @PathVariable String period
    )
    {
        return stockIndicatorRepository.findDistinctTimesForDate(date, period);
    }

    @GetMapping("/multiple-query-aggr/{dateTime}/{period}/{greaterOrLesser}/")
    public Map<String, Object>  findIndicatorsdByMultipleConditionsAggr(
            @PathVariable String dateTime,
            @PathVariable String period,
            @PathVariable String greaterOrLesser,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "20") int size,
            @RequestParam Optional<String> wavePm14,
            @RequestParam Optional<String> wavePm50,
            @RequestParam Optional<String> wavePm100,
            @RequestParam Optional<String> wavePm200,
            @RequestParam Optional<String> cci14,
            @RequestParam Optional<String> cci50,
            @RequestParam Optional<String> cci100,
            @RequestParam Optional<String> cci200
    ) {

        LocalDateTime localDateTime = DateUtil.convertToLocalDateTime(dateTime);
        Pageable paging = PageRequest.of(page, size);

        Optional<Page<StockIndicator>> stockIndicatorPage =
            Optional.ofNullable(stockIndicatorRepository.findIndicatorsdByMultipleConditionsAggr(
                        paging,
                        localDateTime,
                        period,
                        greaterOrLesser,
                        wavePm14,
                        wavePm50,
                        wavePm100,
                        wavePm200,
                        cci14,
                        cci50,
                        cci100,
                        cci200
            ));

        if(stockIndicatorPage.isPresent()) {
            Map<String, Object> response = new HashMap<>();
            response.put("indicators", stockIndicatorPage.get().getContent());
            response.put("currentPage", stockIndicatorPage.get().getNumber());
            response.put("totalItems", stockIndicatorPage.get().getTotalElements());
            response.put("totalPages", stockIndicatorPage.get().getTotalPages());

            return response;

        }else {
            throw new RuntimeException("StockIndicator not found with url ");
        }
    }
}
