package com.bartek.restapi.repository.stockdata;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class StockDataIntraJsonRepositoryImpl implements StockDataIntraJsonRepositoryCustom
{

    private final MongoTemplate mongoTemplate;

    @Autowired
    public StockDataIntraJsonRepositoryImpl(MongoTemplate mongoTemplate)
    {
        this.mongoTemplate = mongoTemplate;
    }

}
