package com.bartek.restapi.repository.stockdata;

import com.bartek.model.morphia.StockDataIntraJson;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface StockDataIntraJsonJsonRepository
        extends MongoRepository<StockDataIntraJson, Long>, StockDataIntraJsonRepositoryCustom
{
}
