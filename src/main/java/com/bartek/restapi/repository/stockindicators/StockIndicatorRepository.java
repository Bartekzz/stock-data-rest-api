package com.bartek.restapi.repository.stockindicators;

import com.bartek.model.morphia.indicators.StockIndicator;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface StockIndicatorRepository
        extends MongoRepository<StockIndicator, Long>, StockIndicatorRepositoryCustom
{
}
