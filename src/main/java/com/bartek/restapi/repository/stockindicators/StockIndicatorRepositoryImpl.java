package com.bartek.restapi.repository.stockindicators;

import com.bartek.model.morphia.indicators.StockIndicator;
import com.bartek.model.morphia.indicators.WavePm;
import com.bartek.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.TypedAggregation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.repository.support.PageableExecutionUtils;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;

@Repository
public class StockIndicatorRepositoryImpl implements StockIndicatorRepositoryCustom
{

    private final MongoTemplate mongoTemplate;

    @Autowired
    public StockIndicatorRepositoryImpl(MongoTemplate mongoTemplate)
    {
        this.mongoTemplate = mongoTemplate;
    }

    @Override
    public Page<StockIndicator> findIndicatorsdByMultipleConditionsAggr(
            Pageable paging,
            LocalDateTime localDateTime,
            String period,
            String greaterOrLesser,
            Optional<String> wavePm14,
            Optional<String> wavePm50,
            Optional<String> wavePm100,
            Optional<String> wavePm200,
            Optional<String> cci14,
            Optional<String> cci50,
            Optional<String> cci100,
            Optional<String> cci200
    ) {


        final List<Criteria> criteriaList = new ArrayList<>();

        Criteria criteria = new Criteria();
        criteria.andOperator(
                Criteria.where("wavePmList.date").gte(localDateTime),
                Criteria.where("wavePmList.date").lte(localDateTime),
                Criteria.where("cciList.date").gte(localDateTime),
                Criteria.where("cciList.date").lte(localDateTime),
                Criteria.where("wavePmList.period").is(period),
                Criteria.where("cciList.period").is(period)
                );
        criteriaList.add(criteria);

        if(greaterOrLesser.equals("gte"))
        {
            wavePm14.ifPresent(wavePm14Value -> criteriaList
                    .add(Criteria.where("wavePmList.wavePm14").gte(new BigDecimal(wavePm14Value))));
            wavePm50.ifPresent(wavePm50Value -> criteriaList
                    .add(Criteria.where("wavePmList.wavePm50").gte(new BigDecimal(wavePm50Value))));
            wavePm100.ifPresent(wavePm100Value -> criteriaList
                    .add(Criteria.where("wavePmList.wavePm100").gte(new BigDecimal(wavePm100Value))));
            wavePm200.ifPresent(wavePm200Value -> criteriaList
                    .add(Criteria.where("wavePmList.wavePm200").gte(new BigDecimal(wavePm200Value))));
            cci14.ifPresent(
                    cci14Value -> criteriaList.add(Criteria.where("cciList.cci14").gte(new BigDecimal(cci14Value))));
            cci50.ifPresent(
                    cci14Value -> criteriaList.add(Criteria.where("cciList.cci50").gte(new BigDecimal(cci14Value))));
            cci100.ifPresent(
                    cci14Value -> criteriaList.add(Criteria.where("cciList.cci100").gte(new BigDecimal(cci14Value))));
            cci200.ifPresent(
                    cci14Value -> criteriaList.add(Criteria.where("cciList.cci200").gte(new BigDecimal(cci14Value))));
        }

        if(greaterOrLesser.equals("lte"))
        {
            wavePm14.ifPresent(wavePm14Value -> criteriaList
                    .add(Criteria.where("wavePmList.wavePm14").lte(new BigDecimal(wavePm14Value))));
            wavePm50.ifPresent(wavePm50Value -> criteriaList
                    .add(Criteria.where("wavePmList.wavePm50").lte(new BigDecimal(wavePm50Value))));
            wavePm100.ifPresent(wavePm100Value -> criteriaList
                    .add(Criteria.where("wavePmList.wavePm100").lte(new BigDecimal(wavePm100Value))));
            wavePm100.ifPresent(wavePm200Value -> criteriaList
                    .add(Criteria.where("wavePmList.wavePm200").lte(new BigDecimal(wavePm200Value))));
            cci14.ifPresent(
                    cci14Value -> criteriaList.add(Criteria.where("cciList.cci14").lte(new BigDecimal(cci14Value))));
            cci50.ifPresent(
                    cci14Value -> criteriaList.add(Criteria.where("cciList.cci50").lte(new BigDecimal(cci14Value))));
            cci100.ifPresent(
                    cci14Value -> criteriaList.add(Criteria.where("cciList.cci100").lte(new BigDecimal(cci14Value))));
            cci200.ifPresent(
                    cci14Value -> criteriaList.add(Criteria.where("cciList.cci200").lte(new BigDecimal(cci14Value))));
        }

        Query query = new Query().with(paging);

        List<AggregationOperation> list = new ArrayList<>();
        list.add(Aggregation.unwind("wavePmList"));
        list.add(Aggregation.unwind("cciList"));
        for(Criteria c : criteriaList) {
            list.add(Aggregation.match(c));
        }
        list.add(Aggregation.group("stockCompany")
                .push("wavePmList").as("wavePmList")
                .push("cciList").as("cciList")
        );
        list.add(Aggregation.project("wavePmList", "cciList").
                and("stockCompany").previousOperation());
        list.add(sort(Sort.Direction.ASC, "stockCompany.symbol"));
        list.add(skip(paging.getOffset()));
        list.add(limit(paging.getPageSize()));
        TypedAggregation<StockIndicator> agg = Aggregation.newAggregation(StockIndicator.class, list);
        List<StockIndicator> resultList = mongoTemplate.aggregate(agg, StockIndicator.class, StockIndicator.class).getMappedResults();

        return PageableExecutionUtils.getPage(resultList, paging,
                () -> mongoTemplate.count((Query.of(query).limit(-1).skip(-1)), StockIndicator.class));
    }

    @Override
    public List<LocalDateTime> findDistinctDateTimes(
            Optional<String> period
    ) {

        Query query = new Query();
        if(period.isPresent()) {
            Criteria criteria = Criteria.where("wavePmList.period").is(period.get());
            query.addCriteria(criteria);
        }

        List<LocalDateTime> list = mongoTemplate.findDistinct(query, "wavePmList.date", StockIndicator.class, LocalDateTime.class);
        return list;
    }

    @Override
    public Set<LocalTime> findDistinctTimesForDate(
            String date,
            String period
    ) {

        LocalDate localDate = DateUtil.convertToLocalDate(date);
        LocalDateTime startOfDay = localDate.atStartOfDay();
        LocalDateTime endOfDay = localDate.atTime(23, 59);

        Criteria criteria = new Criteria();
        criteria.andOperator(
                Criteria.where("wavePmList.date").gte(startOfDay),
                Criteria.where("wavePmList.date").lte(endOfDay),
                Criteria.where("wavePmList.period").is(period)
                );

        List<AggregationOperation> list = new ArrayList<>();
        list.add(Aggregation.unwind("wavePmList"));
        list.add(Aggregation.match(criteria));
        list.add(Aggregation.group().addToSet("wavePmList").as("wavePmList"));
        list.add(Aggregation.project("wavePmList"));
        TypedAggregation<StockIndicator> agg = Aggregation.newAggregation(StockIndicator.class, list);
        List<StockIndicator> resultList = mongoTemplate.aggregate(agg, StockIndicator.class, StockIndicator.class).getMappedResults();

        Set<LocalTime> localDateTimeList = resultList.get(0).getWavePmList().stream()
                .map(x -> x.getDate().toLocalTime()).collect(Collectors.toSet());
        return localDateTimeList;
    }
}
