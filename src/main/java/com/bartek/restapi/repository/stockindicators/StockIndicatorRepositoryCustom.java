package com.bartek.restapi.repository.stockindicators;

import com.bartek.model.morphia.indicators.StockIndicator;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface StockIndicatorRepositoryCustom
{

    Page<StockIndicator> findIndicatorsdByMultipleConditionsAggr(
            Pageable paging,
            LocalDateTime localDateTime,
            String period,
            String greaterOrLesser,
            Optional<String> wavePm14,
            Optional<String> wavePm50,
            Optional<String> wavePm100,
            Optional<String> wavePm200,
            Optional<String> cci14,
            Optional<String> cci50,
            Optional<String> cci100,
            Optional<String> cci200
    );

    List<LocalDateTime> findDistinctDateTimes(
            Optional<String> period
    );

    Set<LocalTime> findDistinctTimesForDate(
            String date,
            String period
    );
}
